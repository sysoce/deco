# Redux Javascript Component

This scaffold aims to be a structure that can be used by any component as a framework in any level of an application.
By using this structure the component will be

Able to integrate smoothly with other components
Run independently
Run as a part in a larger application
Be easily testable

A larger application can use this scaffold as a top level component that uses multiple lower level components.

Any top level folder can be removed if not needed, but all parts of the component should fit into one of these folders.
Every folder should have an index.js file that exports the interface.
The top level index should provide an interface to be used by other components ServiceProviders.


## MODEL
### Store
The Store is the single source of truth for the State of an application.
It also acts as a Pub/Sub and runs Reducers and Middleware in response to Actions.
Provides a "dispatch" function to dispatch actions.
Provides a "subscribe" function to subscribe to state changes.

An application should use one only Store.
However you can have a store in your component for testing purposes or to provide a way to run it independently.

### Reducers
Reducers create the next State of the Store as a result of dispathed Actions.

### Actions
Actions are simple objects that provides a minimal interface to modify the State via the Reducers.
ActionCreators are functions that simply returns an Action and provide an easy way for the rest of the application to use Actions.

### Selectors
Selectors provide a friendly interface to the State of the application.


## VIEW
### View
Here you provide the view layer components. Containers and Presentational components.
View layer components can use Jobs as callbacks.
<!-- The view layer should export a render function that is to be called whenever the state changes. -->


## CONTROLLER

## Features
Runs Jobs to render a feature. Provides a render function to be called whenever the state changes.

## DOMAINS
### Jobs
Jobs dispatches Actions, utilizes Services, Selectors and Views to perform a task.
Jobs should be grouped into Domains representing the task domain the Job is concerned with.
Jobs should do only one thing.
Jobs can be asynchronous.

"A Job is responsible for one element of execution in the application, and play the role of a step in the accomplishment of a Feature. They are the stewards of reusability in our code.

Jobs live inside Domains, which requires them to be abstract, isolated and independent from any other job be it in the same domain or another - whatever the case, no Job should dispatch another Job.

They can be ran by any Feature from any Service, and it is the only way of communication between services and domains."

Example Jobs:
	Save an entry to backend storage.
	Dispatch an Action to change the application State to indicate the entry was saved.
	Get/Select the entry from state.
	Render a View with the entry.

<!-- ### ServiceProviders
Here you provide an interface to other components or Services. -->



TODO:
How to structure and separate presentational and container components from the store
Fix redux dev tools to get last state
Fork Netlify Identity Widget to customize login screen
General solution for custom auth page



Use cases, Features:

Element Interface: Associate an editable element with an ID
	Get an elements ID
	Get all elements that has ID

Editor:
	Attach Editor GUI to elements
	Bind user action to Save action and element
	Get data


Save feature:
	Bind user GUI action to Save action and element
	On save
		Get element data from Editor
		Get ID of element being saved
		Check if data is saved as entity
		Load entity with ID from Backend
		Edit entity with new data
		Save edited entity to Backend




<!-- 1. Get collection and slug data (from DOM or template)
	(build formatted entry data with all entries for each page, then just fetch the file)
2. Load all entries found on page using collection and slug data.
	Show loading screen until cms and all entries loaded
4. When user initiates edit, by clicking an entry field, get the entry's savedData from state and use it to initiate the editor
5. When editor blurs use callback to check if data has changed with EditorContainer and initiate saving
6. Save data by getting the entry from state and the the new data from the EditorContainer, and call the backend
7. When saved change state and savedData in EditorContainer -->
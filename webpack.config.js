const webpack = require('webpack');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const pkg = require('./package.json');
const path = require('path');

const isProduction = process.env.NODE_ENV === 'production';

const stats = () => {
  if (isProduction) {
    return {
      builtAt: false,
      chunks: false,
      colors: true,
      entrypoints: false,
      errorDetails: false,
      hash: false,
      modules: false,
      timings: false,
      version: false,
      warnings: false,
    };
  }
  return {
    all: false,
  };
};

const baseConfig = {
  mode: isProduction ? 'production' : 'development',
  context: path.resolve(__dirname, 'src'),
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'dev-test/static/js'),
    // publicPath: "/public/",
    filename: 'cms.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,

        include: [
          path.resolve(__dirname, "src")
        ],

        exclude: /node_modules/,

        use: {
          loader: 'babel-loader',
          options: {
            configFile: path.resolve(__dirname, 'babel.config.js'),
          },
        },
      },
      {
        test: /\.css$/,
        include: [/(redux-notifications|react-datetime)/],
        use: ['to-string-loader', 'css-loader'],
      },
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      COMPONENT_VERSION: JSON.stringify(`${pkg.version}${isProduction ? '' : '-dev'}`),
    }),
    new FriendlyErrorsWebpackPlugin({
      compilationSuccessInfo: {
        messages: ['Component is now running at http://localhost:8080'],
      },
    }),
  ],
  devtool: isProduction ? 'source-map' : undefined,
  target: 'web',
  stats: stats(),
  devServer: {
    contentBase: path.resolve(__dirname, 'dev-test'),
    inline: true,
    watchContentBase: true,
    quiet: false,
    host: 'localhost',
    port: 8080,
  },
};

module.exports = baseConfig;

import store from '../netlify-cms-core/src/redux'
import { save } from '../actions';

export {selectCollection} from './collections';

export const onSaveAction = (id, data) => {
	store.dispatch(save(id, data));
}


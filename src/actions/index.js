import { loadEntry, changeDraftField, persistEntry, createDraftFromEntry, createEmptyDraft } from '../netlify-cms-core/src/actions/entries'
import { selectCollection } from '../selectors';
import { selectEntry } from '../netlify-cms-core/src/reducers';

export function save(id, data) {
  return (dispatch, getState) => {
    const collection = selectCollection(getState(), id.collection);
    console.log(id)
    console.log(collection)
    dispatch(loadEntry(collection, id.slug)).then((loadedEntry) => {
		console.log('entry loaded')
		console.log(loadedEntry)
		const entry = selectEntry(getState(), id.collection, id.slug);
    	dispatch(createDraftFromEntry(entry));
	}, (result) => {
		console.log('entry not loaded')
		console.log(result.payload.error.status)
		if(result.payload.error.status === 404) {
			dispatch(createEmptyDraft(collection));
			dispatch(changeDraftField('title', id.slug, null));
		}
	}).then(() => {
		dispatch(changeDraftField(id.field, data));
    	dispatch(persistEntry(collection));
	});
  };
}


/* TODO:
	Notifcations: overall control, if not 404
	CKEditor styling
	remove styling, isolate login view
	check if data dirty
*/

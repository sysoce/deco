import bootstrap from './bootstrap';
import registry from './lib/registry';

export { registry as default, bootstrap as init };

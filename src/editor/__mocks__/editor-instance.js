// Import this named export into your test file:

export var testData = "<p>test string 123.</p>";

export const mockGetData = jest.fn();
mockGetData.mockReturnValue( testData );

export const mockSetData = jest.fn();
const mock = jest.fn().mockImplementation(() => {
  return {
  	getData: mockGetData,
  	setData: mockSetData
  };
});

export default mock;
export default function addEventListenerOnce(element, event, fn) {
    var func = function (ev) {
        element.removeEventListener(event, func);
        fn(ev);
    };
    element.addEventListener(event, func);
}
// import mockEditorImplementation, {mockGetData, mockSetData, testData} from '../__mocks__/editor-instance';
// import Editor from '../editor';

it('has_a_test_suite', () => {
	expect(true).toBe(true);
});

// var mockEditorInstance;
// beforeEach(() => {
//   // Clear all instances and calls to constructor and all methods:
//   mockEditorImplementation.mockClear();
//   mockEditorInstance = new mockEditorImplementation();
//   mockGetData.mockClear();
//   mockSetData.mockClear();
// });

// it('throws_error_if_no_instance', () => {
// 	expect(() => {
// 		let editor = new Editor();
// 	}).toThrow();
// })

// it('inits_instance', () => {
// 	let editor = new Editor(mockEditorInstance);
// 	expect(editor.instance).toBe(mockEditorInstance);
// })

// it('inits_saved_data', () => {
// 	let editor = new Editor(mockEditorInstance, testData);
// 	expect(editor.instance).toBe(mockEditorInstance);
// 	expect(editor.savedData).toBe(testData);
// })

// it('sets_saved_data_to_null_if_no_value_passed', () => {
// 	let editor = new Editor(mockEditorInstance);
// 	expect(editor.savedData).toBe(null);
// })

// it('gets_data', () => {
// 	let editor = new Editor(mockEditorInstance);
// 	expect(editor.getData()).toBe(testData);
// 	expect(mockGetData).toHaveBeenCalledTimes(1);
// })

// it('sets_data', () => {
// 	let text1 = "Some text.";
// 	let text2 = "Some other text.";
// 	let editor = new Editor(mockEditorInstance);
// 	editor.setData(text1);
// 	expect(mockSetData).toHaveBeenCalledWith(text1);
// 	editor.setData(text2);
// 	expect(mockSetData).toHaveBeenLastCalledWith(text2);
// 	expect(mockSetData).toHaveBeenCalledTimes(2);
// })

// it('indicates_if_data_is_saved', () => {
// 	let editor = new Editor(mockEditorInstance, testData);
// 	expect(editor.isDataSaved()).toBe(true);
// })

// it('indicates_if_data_is_not_saved', () => {
// 	let editor = new Editor(mockEditorInstance, null);
// 	expect(editor.isDataSaved()).toBe(false);
// })
import addEventListenerOnce from '../event';

const map = {};
beforeEach(() => {
	window.addEventListener = jest.fn((event, cb) => {
		map[event] = cb;
	});
	window.removeEventListener = jest.fn((event, cb) => {
		delete map[event];
	});
});

it('adds_eventlistener', () => {
	let event = { type: 'click', currentTarget: window }
	const cb = jest.fn();
	addEventListenerOnce(window, event.type, cb);
	map[event.type]();
	expect(cb).toHaveBeenCalledTimes(1)
});

it('removes_eventlistener_after_callback', () => {
	let event = { type: 'click', currentTarget: window }
	const cb = jest.fn();
	addEventListenerOnce(window, event.type, cb);
	map[event.type]();
	expect(map[event.type]).toBeUndefined();
});
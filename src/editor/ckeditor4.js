import CKEDITOR from 'ckeditor';

export default function createEditor(element) {
	console.log("createEditor ckeditor4");
	return new Promise(function(resolve, reject) {
		let editor = window.CKEDITOR.inline(element);
		if(editor) resolve(editor);
		reject(new Error("Failed to create editor."));
	});
}
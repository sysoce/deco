import InlineEditor from '@ckeditor/ckeditor5-build-inline';

export default function createEditor(element) {
	console.log("createEditor ckeditor5");
	return InlineEditor.create( element );
}

export function setOnBlur(instance, callback) {
	instance.ui.focusTracker.on( 'change:isFocused', ( evt, name, isFocused ) => {
	    if ( !isFocused ) {
	    	callback(evt, name, isFocused);
	    }
	});
}
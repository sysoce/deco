import { onSaveAction } from '../selectors'
import Editor from './editor'

const ROOT_SELECTOR = '[data-collection][data-slug]';

function getElements() {
  return document.querySelectorAll(ROOT_SELECTOR);
}

function getIdentifier(element) {
  return {
    collection: element.dataset.collection,
    slug: element.dataset.slug,
    field: element.dataset.field ? element.dataset.field : 'body'
  };
}

function init() {
  console.log("init Editor..")
  const elements = getElements();
  console.log(elements)
  for (var i = 0; i < elements.length; i++) {
    let editor = new Editor(
      getIdentifier(elements[i]),
      elements[i],
      onSaveAction
    );
    console.log(editor)
  }
}

export default init;

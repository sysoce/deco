import createEditor, { setOnBlur } from './ckeditor5.js';
import addEventListenerOnce from './event.js';

export default class Editor {
	constructor(id, element, onBlur = null) {
		this.id = id;
		this.element = element;
		this.instance = null;
		if(onBlur) this.setOnBlur(onBlur);
		addEventListenerOnce( element, 'click', this.init.bind(this) );
	}

	init() {
		createEditor(this.element).then( instance => {
			this.instance = instance;
			if(this.onBlur) this.setOnBlur(this.onBlur);
	    } );
	}

	setOnBlur(callback) {
		this.onBlur = callback;
		if(this.instance) setOnBlur(this.instance, () => {
			this.onBlur(this.id, this.getData());
		});
	}

	getData() {
		if(this.instance) return this.instance.getData();
		else return this.element.innerHTML;
	}

	setData(sanitizedData) {
		if (this.instance) this.instance.setData(sanitizedData);
		else this.element.innerHTML = sanitizedData; // DANGER: Exposes risk of XXS !
	}
}
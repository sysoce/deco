'use strict';

const path = require('path');

module.exports = {
  presets: [
  	'@babel/preset-react',
  	'@babel/preset-env'
  ],
  plugins: [
    'emotion',
  	// 'lodash',
    // '@babel/plugin-transform-modules-commonjs',
    // '@babel/plugin-transform-object-assign',
    // [
    //   'babel-plugin-transform-builtin-extend',
    //   {
    //     globals: ['Error'],
    //   },
    // ],
    // 'transform-export-extensions',
    '@babel/plugin-proposal-class-properties',
    // '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-proposal-export-default-from',
    [
      "@babel/plugin-transform-runtime",
      {
        "regenerator": true
      }
    ],
    [
      'module-resolver',
      {
        // root: path.join(__dirname, 'src'),
        alias: {
          src: path.join(__dirname, 'src'),
          // Actions: path.join(__dirname, 'src/actions/'),
          // Constants: path.join(__dirname, 'src/constants/'),
          // Formats: path.join(__dirname, 'src/formats/'),
          // Integrations: path.join(__dirname, 'src/integrations/'),
          // Lib: path.join(__dirname, 'src/lib/'),
          // Reducers: path.join(__dirname, 'src/reducers/'),
          // Redux: path.join(__dirname, 'src/redux/'),
          // Routing: path.join(__dirname, 'src/routing/'),
          // ValueObjects: path.join(__dirname, 'src/valueObjects/'),
        },
      }
    ],
  ]
};
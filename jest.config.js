'use strict';

module.exports = {
    transform: {
        '^.+\\.jsx$': 'babel-jest',
        '^.+\\.js$': 'babel-jest'
    },
    testURL: 'http://localhost:8080',
};